Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: '404',
});

Router.map(function() {
	this.route('home', {
		path: '/',
		onBeforeAction: function () {
			if (!Meteor.userId()) {
		    	// if the user is not logged in, render the Login template
		    	this.render('Login');
			} else {
			    this.next();
			}
		}
	});
});

Router.route('/josh', function() {
	this.render('josh');
});